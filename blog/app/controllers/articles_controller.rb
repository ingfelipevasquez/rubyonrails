class ArticlesController < ApplicationController
	#before_filter :find_model
	#private
	#def find_model
	#	@model = Articles.find(params[:id]) if params[:id]
	#end
	#GET /articles/
	def index
		@articles = Article.all
	end

	#GET /articles/:id
	def show
		@article = Article.find(params[:id])
	end

	#GET /articles/new
	def new
		@article = Article.new
	end

	#GET /articles/create
	def create
		@article = Article.new(article_params)
		if @article.save
			redirect_to @article
		else
			render :new
		end
	end

	#DELETE /articles/:id
	def destroy
		@article = Article.find(params[:id])
		@article.destroy
		redirect_to articles_path
	end

	def edit
		@article = Article.find(params[:id])
	end

	def update
		@article = Article.find(params[:id])
		if @article.update(article_params)
			redirect_to @article
		else
			render :edit
		end
	end

	private
	def article_params
		params.require(:article).permit(:title,:body)
	end
end
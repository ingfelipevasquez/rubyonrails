Rails.application.routes.draw do
  get 'welcome/index'
  resources :articles
=begin
  	GET "/articles" index
  	POST "/articles" create
  	DELETE "/articles/:id" destroy
  	GET "/articles/:id" show
  	GET "/articles/new" new
  	GET "/articles/:id/edit" edit
  	PATCH "/articles/:id" update
  	PUT "/articles/:id" update
=end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'articles#index'
end
